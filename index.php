<!DOCTYPE html>

<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>PRADA</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#0070C0">
        <style>
            body{
                font-family: sans-serif;
            }
            h1, p {
                text-align: center;
            }
            #data_filter{
                display: block;
                width: 100%;
                text-align: center;
                font-weight: bold;
                margin: 20px 0;
            }
            #data_filter input{
                padding: 5px;
                border: 0;
                border-bottom: 2px solid black;
            }
            button{
                cursor: pointer;
                background-color: white;
                border: 0;
                border: 2px solid #484848;
                border-radius: 5px;
                padding: 5px 10px;
            }
            button:hover{
                background-color: #484848;
                color: white;
            }
        </style>
    </head>
    <body>
        <h1>PRADA</h1>
        <p>Personne responsable de l'accès aux documents administratifs et des questions relatives à la réutilisation des informations publiques</p>
        <p><a href="https://www.cada.fr/administration/le-role-de-la-prada" target="_blank" rel="noopener noreferrer">En savoir plus...</a></p>

        <table id="data" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Dép.</th>
                    <th>Administration</th>
                    <th>Prénom</th>
                    <th>Nom</th>
                    <th>Courriel</th>
                </tr>
            </thead>
        </table>
        
        <p><button id="mailto">Mailto</button></p>
        <p><a href="https://www.cada.fr/administration/le-role-de-la-prada" target="_blank" rel="noopener noreferrer">Source des données</a></p>

        <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
        <script type="text/javascript" src="http://dav.li/jquery/3.4.1.min.js"></script>
        <script type="text/javascript" src="DataTables/datatables.min.js"></script>
        <script src="PapaParse-5.0.0/papaparse.min.js"></script>
        <script>
        $.fn.dataTable.ext.errMode = 'none';
        $(document).ready(function() {
            Papa.parse("annuaire_des_prada.csv", {
                download: true,
                complete: function(results) {
                    console.log("Finished:", results.data);

                    var table = $("#data").on( 'error.dt', function ( e, settings, techNote, message ) {
                        console.log( 'An error has been reported by DataTables: ', message );
                    }).DataTable( {
                        data: results.data,
                        select: 'single',
                        pageLength:50,
                        deferRender: true,
                        scrollY: "500px",
                        scrollCollapse: false,
                        scroller: {
                            rowHeight: 28,
                            boundaryScale: 0.75,
                        },
                        pagingType:"simple",
                        language:{
                            //"decimal":",",
                            //"thousands": " ",
                            "emptyTable": "Aucune donnée.",
                            "info": "De _START_ à _END_ sur un total de _TOTAL_ PRADA",
                            "infoEmpty": "Aucune donnée.",
                            "infoFiltered": "(filtré de _MAX_ PRADA totales)",
                            "infoPostFix": "",
                            "thousands": ",",
                            "lengthMenu": "",
                            "loadingRecords": "Chargement en cours...",
                            "processing": "Génération en cours...",
                            "search": "Recherche",
                            "zeroRecords": "Aucun résultat.",
                            "paginate": {
                                "first": "Premier",
                                "last": "Dernier",
                                "next": "Suivant",
                                "previous": "Précédent"
                            },
                            "aria": {
                                "sortAscending":  ": activate to sort column ascending",
                                "sortDescending": ": activate to sort column descending"
                            }
                        }
                    }).draw();
                    var searchResults="";
                    table.on('search.dt', function() {
                        searchResults="";
                        table.rows( { filter : 'applied'} ).data().each(function(data){
                            if(data[4]){
                                searchResults+=data[1]+","+data[4]+"|";
                            }
                        });
                    });
                    $("#mailto").click(function(){
                        window.open("mailto.php?destinataires="+searchResults);
                    });
                }
            });
        } );
        </script>
    </body>
</html>