<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>
<!DOCTYPE html>

<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>PRADA – mailto</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#0070C0">
        <style>
            body{
                font-family: sans-serif;
            }
            h1, p {
                text-align: center;
            }
            button,[type="submit"]{
                cursor: pointer;
                background-color: white;
                border: 0;
                border: 2px solid #484848;
                border-radius: 5px;
                padding: 5px 10px;
            }
            button:hover,[type="submit"]:hover{
                background-color: #484848;
                color: white;
            }
            form{
                border: 1px dashed rgba(0,0,0,0.2);
                padding: 10px;
            }
            label{
                display: block;
                margin-bottom: 10px;
            }
            textarea{
                display: block;
                width: calc(100% - 10px);
                min-height: 100px;
                padding: 5px;
                margin-bottom: 10px;
            }
            form>p{
                text-align: left;
                font-style: italic;
            }
        </style>
    </head>
    <body>
        <h1>Mailto</h1>
        
        <form method="get" action="mailto.php">
            <label for="destinataires">Destinataires</label>
            <textarea name="destinataires"><?php if(!empty($_GET["destinataires"])){
                    echo(htmlspecialchars($_GET["destinataires"], ENT_QUOTES, 'UTF-8'));
                }
            ?></textarea>
            <label>Corps du courriel</label>
            <textarea name="body"><?php if(!empty($_GET["body"])){
                    echo(htmlspecialchars($_GET["body"], ENT_QUOTES, 'UTF-8'));
                }
            ?></textarea>
            <label>Traitement du nom de l'administration</label>
            <textarea name="filtre_admin"><?php if(!empty($_GET["filtre_admin"])){
                    echo(htmlspecialchars($_GET["filtre_admin"], ENT_QUOTES, 'UTF-8'));
                }
            ?></textarea>
            <p>Effectuer une suppression dans l'ordre de tous les éléments séparé par un "|" dans le nom de l'administration. Exemple : "Préfecture de l'|Préfecture du|Préfecture de la|Préfecture de|Préfecture des". Le token %administration% dans le corps du courriel permet de mentionner le nom filtré.</p>
            <input type="submit" value="Mettre à jour"/>
        </form>
        
        <?php if(!empty($_GET["destinataires"])){
                $destinataires=explode("|",$_GET["destinataires"]);
        ?>
        <h2><?php echo(sizeof($destinataires)); ?> courriels à envoyer</h2>
        <button id="ouvrirTout">Tout ouvrir</button>
        <ul>
            <?php 
                foreach($destinataires as $destinataire){
                    if(!empty($destinataire)){
                        $destinataire_array=explode(",",$destinataire);
                        $administration=$destinataire_array[0];
                        $mail=$destinataire_array[1];
                        if(!empty($_GET["filtre_admin"])){
                            $administration_token=$administration;
                            $administration_filtre=explode("|",$_GET["filtre_admin"]);
                            foreach($administration_filtre as $filtre){
                                $administration_token=str_replace($filtre,"",$administration_token);
                            }
                        }else{
                            $administration_token=$administration;
                        }
                        if(!empty($_GET["body"])){
                            $body=str_replace("%administration%",$administration_token,$_GET["body"]);
                        }else{
                            $body="Renseignez le corps du couriel.";
                        }
                        echo('<li><a href="mailto:'.htmlspecialchars($mail, ENT_QUOTES, 'UTF-8', false).'?subject=Demande%20de%20documents%20publics&body='.rawurlencode(html_entity_decode($body)).'">'.htmlspecialchars($administration, ENT_QUOTES, 'UTF-8', false).'</a></li>');
                    }
                }
        ?>
        </ul>
        <?php } ?>
        

        <script type="text/javascript" src="http://dav.li/jquery/3.4.1.min.js"></script>
        <script src="PapaParse-5.0.0/papaparse.min.js"></script>
        <script>
            $("#ouvrirTout").click(function(){
                $("ul a").each(function(){
                    window.open($(this).attr("href"));
                });
            });
        </script>
    </body>
</html>